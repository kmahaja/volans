Update Bios
https://software.intel.com/en-us/flashing-the-bios-on-joule
Add -clear flag to Flag command
"Flash.bat -clear <file.bin>"

Install Joule Ubuntu
Follow: https://www.ubuntu.com/download/core/intel-joule#desktop

Install ROS

Make catkin workspace

In catkin_ws/src/ clone the following repos
volans

sudo apt-get install ros-kinetic-velodyne-pointcloud
sudo apt-get install ros-kinetic-usb-cam
sudo apt-get install ros-kinetic-compressed-image-transport

rosdep install --from-paths /path/to/your/catkin_ws/src --ignore-src

Follow Ros install: https://developer.dji.com/onboard-sdk/documentation/sample-doc/sample-setup.html#ros-oes


sudo usermod -a -G dialout $USER


Make:
catkin_make --pkg usb_cam -DCMAKE_BUILD_TYPE=Release

Prevent capture card dropping
sudo rmmod uvcvideo
sudo modprobe uvcvideo quirks=128 nodrop=1 timeout=6000

export ROS_MASTER_URI='http://<joule-ip>:11311'
