#include "ros/ros.h"
#include "dji_sdk/dji_sdk.h"

#include <geometry_msgs/QuaternionStamped.h>
#include <dji_sdk/SDKControlAuthority.h>

void attitude_callback(const geometry_msgs::QuaternionStamped::ConstPtr& msg)
{

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "telemetry_printout");
    ros::NodeHandle nh;

    // Subscribe to messages from dji_sdk_node
    ros::Subscriber attitudeSub = nh.subscribe("dji_sdk/attitude", 10, &attitude_callback);
    //ros::Subscriber gpsSub      = nh.subscribe("dji_sdk/gps_position", 10, &gps_callback);
    //ros::Subscriber flightStatusSub = nh.subscribe("dji_sdk/flight_status", 10, &flight_status_callback);
    //ros::Subscriber displayModeSub = nh.subscribe("dji_sdk/display_mode", 10, &display_mode_callback);
    //ros::Subscriber localPosition = nh.subscribe("dji_sdk/local_position", 10, &local_position_callback);
}
