#include "ros/ros.h"
#include "dji_sdk/dji_sdk.h"

#include <sensor_msgs/Joy.h>
#include <dji_sdk/SDKControlAuthority.h>
#include <dji_sdk/QueryDroneVersion.h>
#include <dji_sdk/DroneTaskControl.h>
#include <dji_sdk/SetLocalPosRef.h>

ros::ServiceClient sdkCtrlAuthorityService;
ros::ServiceClient setLocalPosReference;
ros::ServiceClient droneTaskService;
ros::ServiceClient queryVersionService;


ros::Publisher ctrlSetPointPub;
ros::Publisher ctrlXYOffZYawPub;
ros::Publisher ctrlVelocityPub;
ros::Publisher ctrlRPZYPub;

std::string setPointTopic  = "/dji_sdk/flight_control_setpoint_generic";
std::string XYOffZYawTopic = "/dji_sdk/flight_control_setpoint_ENUposition_yaw";
std::string velocityTopic  = "/dji_sdk/flight_control_setpoint_ENUvelocity_yawrate";
std::string RPZYTopic      = "/dji_sdk/flight_control_setpoint_rollpitch_yawrate_zposition";

bool obtainControl()
{
    dji_sdk::SDKControlAuthority authority;
    authority.request.control_enable=1;
    sdkCtrlAuthorityService.call(authority);

    if(!authority.response.result) return false;

    return true;
}

bool setLocalPosition()
{
    dji_sdk::SetLocalPosRef localPosReferenceSetter;
    setLocalPosReference.call(localPosReferenceSetter);
    return localPosReferenceSetter.response.result;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "controller");
    ros::NodeHandle nh;

    // Control Type Publishers
    ctrlSetPointPub  = nh.advertise<sensor_msgs::Joy>(setPointTopic, 10);
    ctrlXYOffZYawPub = nh.advertise<sensor_msgs::Joy>(XYOffZYawTopic, 10);
    ctrlVelocityPub  = nh.advertise<sensor_msgs::Joy>(velocityTopic, 10);
    ctrlRPZYPub      = nh.advertise<sensor_msgs::Joy>(RPZYTopic, 10);

    // Services
    sdkCtrlAuthorityService = nh.serviceClient<dji_sdk::SDKControlAuthority>("dji_sdk/sdk_control_authority");
    droneTaskService        = nh.serviceClient<dji_sdk::DroneTaskControl>("dji_sdk/drone_task_control");
    queryVersionService     = nh.serviceClient<dji_sdk::QueryDroneVersion>("dji_sdk/query_drone_version");

    // Subscriptions to this controller

    // Get control of vehicle
    if(obtainControl()) {
        ROS_ERROR("obtain control failed!");
        return 1;
    }

    // Simple check for gps convergence
    if (!setLocalPosition()) {
        ROS_ERROR("GPS health insufficient - No local frame reference for height. Exiting.");
        return 1;
    }
}
